# Zadání úkolů

1. Změřte časy.
    + V metodě main nechte naplnit pole čísel od 1 do 500 000 000.
    + Změřte čas 3x a zapište si to. Ze 3 naměřených časů udělejte průměr.
    + Udělejte třídu MyThread, která dědí od Thread.
        + Metoda run bude implementovat stejnou logiku, co předešlá metoda main.
        + 500 000 000 se rozdělí mezi 4 vlákna
        + Změřte časy. Udělejte sumu. 
        + Aplikujte 3x.
        + Udělejte průměr a porovnejte s jedním vláknem.
2. Producer-Consumer - https://www.geeksforgeeks.org/producer-consumer-solution-using-threads-java/
    + Implementovat
    + Zaměňte generování čísla u producenta za abecedu (A-Z)
    + Upravte consumera pro abecedu
    + Po vypsání celé abecedy ukončete celý program
    + Vysvětlete kód
