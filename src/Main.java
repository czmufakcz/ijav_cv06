
public class Main {

    public static void main(String[] args) {

        int[] array = new int[MyThread.COUNT];

        Timer timer = new Timer();
        timer.start();
        for (int i = 0; i < MyThread.COUNT; i++) {
            array[i] = i;
        }

        timer.stop();
        System.out.println("Main-time:" + timer.getExecutionTime() + "ms");

        /*
         * for (int i = 0; i < MyThread.COUNT_THREAD; i++) { MyThread myThread = new
         * MyThread(); myThread.start(); }
         */

    }
}
