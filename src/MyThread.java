
public class MyThread extends Thread {

    public static final int COUNT = 300000000;
    public static final int COUNT_THREAD = 3;
    
    private final int[] array;
    
    public MyThread() {
        this.array = new int[COUNT/COUNT_THREAD];
    }


    @Override
    public void run() {
        super.run();
        Timer timer = new Timer();
        timer.start();
        for (int i = 0; i < MyThread.COUNT/COUNT_THREAD; i++) {
            array[i] = i;
        }
        timer.stop();
        System.out.println("Thread-time:" + timer.getExecutionTime() + "ms");
    }
    
    
}
