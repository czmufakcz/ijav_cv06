
// Java program to implement solution of producer 
// consumer problem. 
import java.util.LinkedList;

public class Threadexample {

    private static final int COUNT_CONSUMER = 1;
    private static final int COUNT_PRODUCER = 1;

    public static void main(String[] args) throws InterruptedException {
        // Object of a class that has both produce()
        // and consume() methods
        final PC pc = new PC();

        for (int i = 0; i < Threadexample.COUNT_PRODUCER; i++) {
            Thread thread = new Thread(() -> {
                try {
                    pc.produce();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            });
            thread.start();
        }

        for (int i = 0; i < Threadexample.COUNT_CONSUMER; i++) {
            Thread thread = new Thread(() -> {
                try {
                    pc.consume();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            });
            thread.start();
        }

    }

    // This class has a list, producer (adds items to list
    // and consumber (removes items).
    public static class PC {
        // Create a list shared by producer and consumer
        // Size of list is 2.
        LinkedList<Character> list = new LinkedList<>();
        int capacity = 2;

        // Function called by producer thread
        public void produce() throws InterruptedException {
            char value = 'a';
            while (value != 'z' +1) {
                synchronized (this) {
                    // producer thread waits while list
                    // is full
                    while (list.size() == capacity)
                        wait();

                    System.out.println("Producer -" + value);

                    // to insert the jobs in the list
                    list.add(value++);

                    // notifies the consumer thread that
                    // now it can start consuming
                    notifyAll();

                    // makes the working of program easier
                    // to understand
                    // Thread.sleep(1000);
                }
            }
        }

        // Function called by consumer thread
        public void consume() throws InterruptedException {
            char val = ' ';
            while (val != 'z') {
                synchronized (this) {
                    // consumer thread waits while list
                    // is empty
                    while (list.size() == 0)
                        wait();

                    // to retrive the ifrst job in the list
                    val = list.removeFirst();

                    System.out.println("Consumer -" + val);

                    // Wake up producer thread
                    notify();

                    // and sleep
                    // Thread.sleep(1000);
                }
            }
        }
    }
}
