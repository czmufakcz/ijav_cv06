
public class Timer {
    private long startTime;
    private long endTime;
    
    public Timer() {
        
    }
    
    public void start() {
        this.startTime = System.currentTimeMillis();
    }
    
    public void stop() {
        this.endTime = System.currentTimeMillis();
    }
    
    public long getExecutionTime() {
        return this.endTime - this.startTime;
    }
}
